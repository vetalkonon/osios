angular.module('osios')
    .controller('stage2Ctrl', ['$scope', '$http', '$state', '$stateParams',
      function ($scope, $http, $state, $stateProvider) {
        $scope.projectName = $state.params.stage1Obj.projName;
        $scope.filesListIsEmpty = true;
        // Data from previous stage
        $scope.progressBar = 50;
        $scope.leadClients = $state.params.stage1Obj.leadClients;
        $scope.leadDevs = $state.params.stage1Obj.leadDevelopers;
        $scope.bOptions = $state.params.stage1Obj.briefOptions;
        $scope.projPlatforms = $state.params.stage1Obj.projPlatforms;
        $scope.selectedDevices = $state.params.stage1Obj.selectedDevices;
        // Stage 2 data
        $scope.filePicker = null; //File object model
        $scope.projDesc = '';
        $scope.intendedUsers = '';
        $scope.desDirection = '';
        $scope.colors = [];
        $scope.desiredFonts = '';
        $scope.techReqs = '';
        $scope.filesList = [];
        $scope.notes = '';
        $scope.interface = {};

        if ($state.params.stage2Obj) {
          $scope.projDesc = $state.params.stage2Obj.projDesc;
          $scope.intendedUsers = $state.params.stage2Obj.intendedUsers;
          $scope.desDirection = $state.params.stage2Obj.desDirection;
          $scope.colors = $state.params.stage2Obj.colors;
          $scope.desiredFonts = $state.params.stage2Obj.desiredFonts;
          $scope.techReqs = $state.params.stage2Obj.techReqs;
          $scope.notes = $state.params.stage2Obj.notes;

        }
        
        $scope.deleteColor = function (color) {
          $scope.colors.splice($scope.colors.indexOf(color), 1);
        };

        $scope.addColor = function () {
          this.newColor = {
            value: '#A0A1A3'
          };
          $scope.colors.push(this.newColor);
        };

        //  File upload
        $scope.$on('$dropletReady', function whenDropletReady() {
          // Directive's interface is ready to be used...
          $scope.interface.allowedExtensions(['png', 'jpg', 'pdf', 'psd']);
          $scope.FILE_TYPES = {VALID: 1, INVALID: 2, DELETED: 4, UPLOADED: 8};
          if ($state.params.stage2Obj) {
            $state.params.stage2Obj.files.forEach(function (elem) {
              $scope.interface.addFile(elem.file ? elem.file : elem, $scope.FILE_TYPES.VALID);
            });
          }
          $scope.interface.getFiles($scope.interface.FILE_TYPES.VALID);
        });
        $scope.$on('$dropletFileAdded', function whenDropletFileAdded() {
          // Watch for file Change
          if ($scope.interface.getFiles($scope.interface.FILE_TYPES.VALID).length > 0) {
            $scope.filesListIsEmpty = false;
          } else {
            $scope.filesListIsEmpty = true;
          }
          console.dir($scope.interface.getFiles($scope.interface.FILE_TYPES.VALID))
          $scope.watchForChanges();
        });

        // Watch for projDesc Change
        $scope.$watch('projDesc', function () {
          if ($scope.projDesc <= 0 || $scope.projDesc == null) {
            $scope.projDescIsEmpty = true;
          } else {
            $scope.projDescIsEmpty = false;
          }
          $scope.watchForChanges();
        });
        // Watch for intendedUsers Change
        $scope.$watch('intendedUsers', function () {
          if ($scope.intendedUsers <= 0 || $scope.intendedUsers == null) {
            $scope.intendedUsersIsEmpty = true;
          } else {
            $scope.intendedUsersIsEmpty = false;
          }
          $scope.watchForChanges();
        });
        // Watch for desDirection Change
        $scope.$watch('desDirection', function () {
          if ($scope.desDirection <= 0 || $scope.desDirection == null) {
            $scope.desDirectionIsEmpty = true;
          } else {
            $scope.desDirectionIsEmpty = false;
          }
          $scope.watchForChanges();
        });
        // Watch for desiredFonts Change
        $scope.$watch('desiredFonts', function () {
          if ($scope.desiredFonts <= 0 || $scope.desiredFonts == null) {
            $scope.desiredFontsIsEmpty = true;
          } else {
            $scope.desiredFontsIsEmpty = false;
          }
          $scope.watchForChanges();
        });
        // Watch for techReqs Change
        $scope.$watch('techReqs', function () {
          if ($scope.techReqs <= 0 || $scope.techReqs == null) {
            $scope.techReqsIsEmpty = true;
          } else {
            $scope.techReqsIsEmpty = false;
          }
          $scope.watchForChanges();
        });
        // Watch for notes Change
        $scope.$watch('notes', function () {
          if ($scope.notes <= 0 || $scope.notes == null) {
            $scope.notesIsEmpty = true;
          } else {
            $scope.notesIsEmpty = false;
          }
          $scope.watchForChanges();
        });
        // Watch for colors Change
        $scope.$watch('colors', function () {
          if ($scope.colors.length > 0) {
            $scope.colorsIsEmpty = false;
          } else {
            $scope.colorsIsEmpty = true;
          }
          $scope.watchForChanges();
        }, true);

        // Set progress bar
        $scope.watchForChanges = function () {
          $scope.progressBar = 0;
          if ($state.params.stage1Obj.projName.length > 0 &&
              $state.params.stage1Obj.leadClients.length > 0 &&
              $state.params.stage1Obj.leadDevelopers.length > 0) {
            $scope.progressBar += 25;
          }
          if ($state.params.stage1Obj.briefOptions.length > 0 &&
              $state.params.stage1Obj.projPlatforms.length > 0 &&
              $state.params.stage1Obj.selectedDevices.length > 0) {
            $scope.progressBar += 25;
          }
          if ($scope.projDesc.length > 0 &&
              $scope.intendedUsers.length > 0 &&
              $scope.desDirection.length > 0 &&
              $scope.colors.length > 0) {
            $scope.progressBar += 25;
          }
          if ($scope.desiredFonts.length > 0 &&
              $scope.techReqs.length > 0 &&
              $scope.notes.length > 0 && !$scope.filesListIsEmpty) {
            $scope.progressBar += 25;
          }

        };
        // State change to final
        // Delete all info
        $scope.clearData = function () {
          location = document.referrer ? document.referrer : '/mobileprojects/';
        };

        $scope.prevStage = function () {
          $state.go('stage1', {
            stage1Obj: $state.params.stage1Obj,
            stage2Obj: {
              projDesc: $scope.projDesc,
              intendedUsers: $scope.intendedUsers,
              desDirection: $scope.desDirection,
              colors: $scope.colors,
              desiredFonts: $scope.desiredFonts,
              techReqs: $scope.techReqs,
              notes: $scope.notes,
              files: $scope.interface.getFiles($scope.interface.FILE_TYPES.VALID)
            }
          });
        };

        $scope.finalStage = function () {
          if ($scope.progressBar === 100) {
            $state.go('stageFinal', {
              stage2Obj: {
                projName: $scope.projectName,
                leadClients: $scope.leadClients,
                leadDevelopers: $scope.leadDevs,
                briefOptions: $scope.bOptions,
                projPlatforms: $scope.projPlatforms,
                selectedDevices: $scope.selectedDevices,
                projDesc: $scope.projDesc,
                intendedUsers: $scope.intendedUsers,
                desDirection: $scope.desDirection,
                colors: $scope.colors,
                desiredFonts: $scope.desiredFonts,
                techReqs: $scope.techReqs,
                notes: $scope.notes,
                files: $scope.interface.getFiles($scope.interface.FILE_TYPES.VALID)
              }
            });
          }
        };


        if (!$state.params.stage1Obj.isLoadedProj) {
          var result = window.location.pathname.match(/project\/(\d+)-/);
          if (!result) {
            return;
          }
          var projectId = result[1];

          var data = new FormData();
          data.append('projectId', projectId);
          $http({
            url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/scripts/get-project-json.php',
            method: "POST",
            data: data,
            headers: {
              'Content-Type': undefined
            }
          })
              .then(function (res) {

                if (res.data != null) {

                  // Get cards data
                  $http({
                    method: 'GET'
                    , url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/templates/add-project/data/cards.json'
                  }).then(function successCallback(cards) {
                    $scope.cardsList = cards.data;

                    $scope.projectName = $state.params.stage1Obj.projName = res.data.name;
                    $state.params.stage1Obj.leadClients = [res.data.leadClient];
                    $state.params.stage1Obj.leadDevelopers = [res.data.leadDeveloper];
                    $state.params.stage1Obj.projPlatforms = $scope.cardsList.filter(function (elem) {
                      return elem.section == 'project platform' && elem.name.toLowerCase() == res.data.platform;
                    });
                    $state.params.stage1Obj.selectedDevices = $scope.cardsList.filter(function (elem) {
                      return elem.section == 'device' && elem.key.toLowerCase() == res.data.device;
                    });
                    $scope.projDesc = res.data.description;
                    $scope.intendedUsers = res.data.intendedUsers;
                    $scope.desDirection = res.data.direction;
                    $scope.colors = res.data.colors.map(function (elem) {
                      return {value: elem}
                    });
                    $scope.desiredFonts = res.data.fonts;
                    $scope.notes = res.data.additionalReq;

                  }, function (res) {

                  });
                }
                $state.params.stage1Obj.isLoadedProj = true;
              }, function (res) {
                $state.params.stage1Obj.isLoadedProj = true;
              });


        }
      }]);

 