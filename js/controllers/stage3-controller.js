angular.module('osios')
    .controller('stage3Ctrl', ['$scope', '$http', '$state', '$stateParams', function ($scope, $http, $state, $stateProvider) {
      $scope.finalData = $state.params.stage2Obj;
      $scope.progressbar = 0;
      $scope.message = 'Your new project is being created';
      var result = window.location.pathname.match(/project\/(\d+)-/);
      if (result) {
        var projectId = result[1];

        $scope.message = 'Your project is being updated';
      }
      var data = new FormData();
      $scope.finalData.files.forEach(function (elem) {
        data.append('file[]', elem.file, elem.file.name);
      });

      $http({
        url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/scripts/upload_design_front.php'
        , method: "POST"
        , data: data
        , headers: {
          'Content-Type': undefined
        }
      }).then(function (res) {
        $http({
          url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/scripts/get-current-user.php'
          , method: "POST"
        }).then(
            function (resUser) {

              data = new FormData();
              data.append('project', $scope.finalData.projName);
              data.append('lead_client', $scope.finalData.leadClients.find(function () {
                return true;
              }).id);
              data.append('lead_developer', $scope.finalData.leadDevelopers.find(function () {
                return true;
              }).id);
              data.append('platform', $scope.finalData.projPlatforms.find(function () {
                return true;
              }).name.toLowerCase());
              data.append('device', $scope.finalData.selectedDevices.find(function () {
                return true;
              }).key);
              data.append('abstracts', $scope.finalData.projDesc);
              data.append('customer', $scope.finalData.intendedUsers);
              $scope.finalData.colors.forEach(function (elem, index) {
                data.append('colour_' + (index<2?(index == 0 ? 'first' : 'second'):(index+1)) + '_color', elem.value.replace('#', ''));
              });
              data.append('fonts', $scope.finalData.desiredFonts);
              data.append('more', $scope.finalData.desDirection);
              data.append('additional_requirements', $scope.finalData.notes);
              data.append('appicon', 'Yes');

              if (projectId) {
                data.append('id', projectId);
              }

              res.data.split(' ')
                  .filter(function (el) {
                    return el.length != 0
                  })
                  .forEach(function (elem, index) {
                    data.append('design_item' + (index + 1), '/mobileprojects/wp-content/uploads/design/' + resUser.data + '/' + elem);
                  });

              processProject(data);
            },
            function (res) {

            }
        );
      }, function (res) {
        console.error(res);
      });

      function processProject(data) {
        $http({
          url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/scripts/process-project.php'
          , method: "POST"
          , data: data,
          headers: {
            'Content-Type': undefined
          }
        })
            .then(function (res) {
              var interval = setInterval(function () {
                $scope.progressbar += 10;
                $scope.$apply();
                if ($scope.progressbar >= 100) {
                  clearInterval(interval);
                  setInterval(function () {
                    location = '/mobileprojects';
                  }, 500);
                }
              }, 100);
            }, function (res) {

            });
      }
    }]);