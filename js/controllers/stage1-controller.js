angular.module('osios')
    .controller('stage1Ctrl', ['$scope', '$http', '$state', '$stateParams',
      function ($scope, $http, $state, $stateParams) {
        // $scope.projectName = '';
        $scope.projectName = $state.params.stage1Obj.projName;
        $scope.leadUsers = [];
        $scope.cardsList = [];
        $scope.progressBar = 0;
        $scope.nameIsEmpty = true;
        $scope.clientsIsEmpty = true;
        $scope.devsIsEmpty = true;
        // Get users data
        $http({
          method: 'POST',
          url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/scripts/get-people-json.php'
        })
            .then(function successCallback(response) {
              $scope.leadUsers = response.data;
            }, function errorCallback(response) {
              console.log(response.data);
            });
        // Get cards data
        $http({
          method: 'GET',
          url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/templates/add-project/data/cards.json'
        })
            .then(function successCallback(cards) {
              $scope.cardsList = cards.data;

              if (!$state.params.stage1Obj.isLoadedProj) {
                var result = window.location.pathname.match(/project\/(\d+)-/);
                if (!result) {
                  return;
                }
                var projectId = result[1];

                var data = new FormData();
                data.append('projectId', projectId);
                $http({
                  url: '/mobileprojects/wp-content/plugins/OSIOS-plugin/scripts/get-project-json.php',
                  method: "POST",
                  data: data,
                  headers: {
                    'Content-Type': undefined
                  }
                })
                    .then(function (res) {

                      if (res.data != null) {
                        $scope.projectName = res.data.name;
                        $scope.leadClients = [res.data.leadClient];
                        $scope.leadDevs = [res.data.leadDeveloper];
                        $scope.projPlatforms = $scope.cardsList.filter(function (elem) {
                          return elem.section == 'project platform' && elem.name.toLowerCase() == res.data.platform;
                        });
                        $scope.selectedDevices = $scope.cardsList.filter(function (elem) {
                          return elem.section == 'device' && elem.key.toLowerCase() == res.data.device;
                        });
                        $state.params.stage2Obj.projDesc = res.data.description;
                        $state.params.stage2Obj.intendedUsers = res.data.intendedUsers;
                        $state.params.stage2Obj.desDirection = res.data.direction;
                        $state.params.stage2Obj.colors = res.data.colors.map(function (elem) {
                          return {value: elem}
                        });
                        $state.params.stage2Obj.desiredFonts = res.data.fonts;
                        $state.params.stage2Obj.notes = res.data.additionalReq;
                        
                      }
                      $state.params.stage1Obj.isLoadedProj = true;
                    }, function (res) {
                      $state.params.stage1Obj.isLoadedProj = true;
                    });

              }

            }, function errorCallback(cards) {
              console.log(cards.data);
            });
        // Add cleients or developers to new project
        $scope.leadClients = $state.params.stage1Obj.leadClients;
        $scope.leadDevs = $state.params.stage1Obj.leadDevelopers;
        $scope.addClient = function (client) {
          var curClientIndex = $scope.leadClients.findIndex(function (elem) {
            return elem.id == client.id
          });
          if (curClientIndex == -1) {
            if ($scope.leadClients.length > 0) {
              $scope.leadClients = [];
            }
            $scope.leadClients.push(client);
          } else {
            $scope.leadClients.splice(curClientIndex, 1);
          }
        };
        $scope.addDev = function (developer) {
          var curDevIndex = $scope.leadDevs.findIndex(function (elem) {
            return elem.id == developer.id
          });
          if (curDevIndex == -1) {
            if ($scope.leadDevs.length > 0) {
              $scope.leadDevs = [];
            }
            $scope.leadDevs.push(developer);
          } else {
            $scope.leadDevs.splice(curDevIndex, 1);
          }
        };

        $scope.isChecked = function (fieldName, elem) {
          return !!$scope[fieldName].find(function (el) {
            return el.id == elem.id
          });
        };
        $scope.filterDevices = function (elem) {
          return elem.section === 'device'
              && $scope.projPlatforms.some(function (el) {
                return el.name.toLowerCase() === elem.platform;
              });
        };
        // Add cards to list
        $scope.bOptions = $state.params.stage1Obj.briefOptions;
        $scope.projPlatforms = $state.params.stage1Obj.projPlatforms;
        $scope.selectedDevices = $state.params.stage1Obj.selectedDevices;
        // --------------------------------------------------
        $scope.addBOptions = function (bOption) {
          var curBOptionIndex = $scope.bOptions.findIndex(function (elem) {
            return elem.id == bOption.id
          });
          if (curBOptionIndex == -1) {
            if ($scope.bOptions.length > 0) {
              $scope.bOptions = [];
            }
            $scope.bOptions.push(bOption);
          } else {
            $scope.bOptions.splice(curBOptionIndex, 1);
          }
        };
        // --------------------------------------------------
        $scope.addProjPlatforms = function (pPlatform) {
          $scope.selectedDevices = [];
          var curProjPlatformsIndex = $scope.projPlatforms.findIndex(function (elem) {
            return elem.id == pPlatform.id
          });
          if (curProjPlatformsIndex == -1) {
            if ($scope.projPlatforms.length > 0) {
              $scope.projPlatforms = [];
            }
            $scope.projPlatforms.push(pPlatform);
          } else {
            $scope.projPlatforms.splice(curProjPlatformsIndex, 1);
            $scope.selectedDevices = $scope.selectedDevices.filter(function (elem) {
              return $scope.filterDevices(elem)
            });
          }
        };
        // --------------------------------------------------
        $scope.addSelectedDevices = function (selectedDevice) {
          var curSelectedDevicesIndex = $scope.selectedDevices.findIndex(function (elem) {
            return elem.id == selectedDevice.id
          });
          if (curSelectedDevicesIndex == -1) {
            if ($scope.selectedDevices.length > 0) {
              $scope.selectedDevices = [];
            }
            $scope.selectedDevices.push(selectedDevice);
          } else {
            $scope.selectedDevices.splice(curSelectedDevicesIndex, 1);
          }
        };
        // -----------------------Progress Bar realisation-----------------------!
        // Watch for ProjectName Change
        $scope.$watch('projectName', function () {
          if ($scope.projectName <= 0 || $scope.projectName == null) {
            $scope.nameIsEmpty = true;
          } else {
            $scope.nameIsEmpty = false;
          }
          $scope.watchForChanges();
        });
        // Watch for Lead Clients Change
        $scope.$watch('leadClients', function () {
          if ($scope.leadClients.length > 0) {
            $scope.clientsIsEmpty = false;
          } else {
            $scope.clientsIsEmpty = true;
          }
          $scope.watchForChanges();
        }, true);
        // Watch for Lead Developers Change
        $scope.$watch('leadDevs', function () {
          if ($scope.leadDevs.length > 0) {
            $scope.devsIsEmpty = false;
          } else {
            $scope.devsIsEmpty = true;
          }
          $scope.watchForChanges();
        }, true);

        // Watch for Lead Brief Options Change
        $scope.$watch('bOptions', function () {
          if ($scope.bOptions.length > 0) {
            $scope.bOptionsIsEmpty = false;
          } else {
            $scope.bOptionsIsEmpty = true;
          }
          $scope.watchForChanges();
        }, true);

        // Watch for Project Platforms Change
        $scope.$watch('projPlatforms', function () {
          if ($scope.projPlatforms.length > 0) {
            $scope.projPlatformsIsEmpty = false;
          } else {
            $scope.projPlatformsIsEmpty = true;
          }
          $scope.watchForChanges();
        }, true);

        // Watch for Selected Devices Change
        $scope.$watch('selectedDevices', function () {
          if ($scope.selectedDevices.length > 0) {
            $scope.selectedDevicesIsEmpty = false;
          } else {
            $scope.selectedDevicesIsEmpty = true;
          }
          $scope.watchForChanges();
        }, true);
        // Set progress bar
        $scope.watchForChanges = function () {
          $scope.progressBar = 0;
          if ($scope.projectName.length > 0 &&
              $scope.leadClients.length > 0 &&
              $scope.leadDevs.length > 0) {
            $scope.progressBar += 25;
          }
          if ($scope.bOptions.length > 0 &&
              $scope.projPlatforms.length > 0 &&
              $scope.selectedDevices.length > 0) {
            $scope.progressBar += 25;
          }
          if ($state.params.stage2Obj.projDesc.length > 0 &&
              $state.params.stage2Obj.intendedUsers.length > 0 &&
              $state.params.stage2Obj.desDirection.length > 0 &&
              $state.params.stage2Obj.colors.length > 0) {
            $scope.progressBar += 25;
          }
          if ($state.params.stage2Obj.desiredFonts.length > 0 &&
              $state.params.stage2Obj.techReqs.length > 0 &&
              $state.params.stage2Obj.notes.length > 0 &&
              $state.params.stage2Obj.files.length > 0) {
            $scope.progressBar += 25;
          }
          /*var stage2Progress = $state.params.stage2Obj ? $state.params.stage2Obj.progressBar : 0;
           if (!$scope.nameIsEmpty && !$scope.clientsIsEmpty && !$scope.devsIsEmpty) {
           $scope.progressBar = stage2Progress + 25;
           if (!$scope.bOptionsIsEmpty && !$scope.projPlatformsIsEmpty && !$scope.selectedDevicesIsEmpty) {
           $scope.progressBar = stage2Progress + 50;
           }
           } else {
           $scope.progressBar = stage2Progress + 0;
           if (!$scope.bOptionsIsEmpty && !$scope.projPlatformsIsEmpty && !$scope.selectedDevicesIsEmpty) {
           $scope.progressBar = stage2Progress + 25;
           }
           }*/
        };
        // Delete all info
        $scope.clearData = function () {
          location = document.referrer ? document.referrer : '/mobileprojects/';
        };
        // State change
        $scope.nextStage = function () {
          var stage2Progress = $state.params.stage2Obj ? $state.params.stage2Obj.progressBar : 0;
          if ($scope.progressBar >= 50) {
            $state.go('stage2', {
              stage1Obj: Object.assign({}, $state.params.stage1Obj, {
                projName: $scope.projectName,
                leadClients: $scope.leadClients,
                leadDevelopers: $scope.leadDevs,
                briefOptions: $scope.bOptions,
                projPlatforms: $scope.projPlatforms,
                selectedDevices: $scope.selectedDevices
              }),
              stage2Obj: $state.params.stage2Obj
            });
          }
        };
      }]);
 