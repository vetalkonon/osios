angular.module('osios', ['ui.router', 'ngAnimate', 'anim-in-out', 'ngDroplet'])
    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
      function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state('stage1', {
              url: "/",
              views: {
                /*"navigation": {
                  templateUrl: 'partials/nav.html'//"/mobileprojects/wp-content/plugins/OSIOS-plugin/templates/add-project/partials/nav.html"
                },*/
                "page-content": {
                  templateUrl: "/mobileprojects/wp-content/plugins/OSIOS-plugin/templates/add-project/partials/main.html"
                },
                controller: 'stage1Ctrl'
              },
              params: {
                stage1Obj: {
                  projName: '',
                  leadClients: [],
                  leadDevelopers: [],
                  briefOptions: [],
                  projPlatforms: [],
                  selectedDevices: [],
                  progressBar:0
                },
                stage2Obj: {

                  projDesc: '',
                  intendedUsers: '',
                  desDirection: '',
                  colors: [],
                  desiredFonts: '',
                  techReqs: '',
                  notes: '',
                  files: [],
                  progressBar:0
                }
              }
            })
            .state('stage2', {
              url: "/stage2",
              views: {
                /*"navigation": {
                  templateUrl: 'partials/nav.html'//"/mobileprojects/wp-content/plugins/OSIOS-plugin/templates/add-project/partials/nav.html"
                },*/
                "page-content": {
                  templateUrl: "/mobileprojects/wp-content/plugins/OSIOS-plugin/templates/add-project/partials/stage2.html"
                },
                controller: 'stage2Ctrl'
              },
              params: {
                stage1Obj: {
                  projName: '',
                  leadClients: [],
                  leadDevelopers: [],
                  briefOptions: [],
                  projPlatforms: [],
                  selectedDevices: [],
                  progressBar:0
                },
                stage2Obj: {

                  projDesc: '',
                  intendedUsers: '',
                  desDirection: '',
                  colors: [],
                  desiredFonts: '',
                  techReqs: '',
                  notes: '',
                  files: [],
                  progressBar:0
                }
              }
            })
            .state('stageFinal', {
              url: "/stage3",
              views: {
                "page-content": {
                  templateUrl: "/mobileprojects/wp-content/plugins/OSIOS-plugin/templates/add-project/partials/stage3.html"
                },
                controller: 'stage3Ctrl'
              },
              params: {
                stage2Obj: {
                  projName: '',
                  leadClients: [],
                  leadDevelopers: [],
                  briefOptions: [],
                  projPlatforms: [],
                  selectedDevices: [],
                  projDesc: '',
                  intendedUsers: '',
                  desDirection: '',
                  colors: [],
                  desiredFonts: '',
                  techReqs: '',
                  notes: ''
                }
              }
            });
      }]);
