angular.module('osios')
    .directive("openDropdown", ["$interval", function () {
      return {
        restrict: "A",
        link: function (scope, elem, attrs) {
          jQuery(function ($) {
            $(elem).click(function () {
              $('.dropdown-button').dropdown({
                constrain_width: false
              });
            });
          });
        }
      }
    }]);
