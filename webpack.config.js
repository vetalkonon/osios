module.exports = {
  context: __dirname + "/js",
  entry: [
    "./libraries/angular.min.js",
    "./libraries/angular-animate.js",
    "./libraries/angular-ui-router.min.js",
    "./libraries/anim-in-out.js",
    "./app.js",
    "./directives/ng-droplet.min.js",
    "./controllers/stage1-controller.js",
    "./controllers/stage2-controller.js",
    "./controllers/stage3-controller.js"
  ],

  output: {
    path: __dirname + "/js",
    filename: "bundle.js"
  }
};